(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (misterioso)))
 '(doc-view-continuous t)
 '(org-agenda-files
   (quote
    ("~/org/" "~/research/stochastic_thermo/" "~/research/stochastic_thermo/periodic_driving/" "/home/danilo/Dropbox/thesis/wet_granular_billiard/thesis.org")))
 '(org-support-shift-select t)
 '(package-selected-packages
   (quote
    (ein ein-subpackages ein-notebook htmlize cua ox-reveal markdown-mode+ markdown-preview-mode markdown-mode ess julia-mode elpy flycheck yasnippet pyvenv gnuplot-mode use-package seq telepathy log4e js3-mode irony-eldoc gntp company-irony-c-headers company-irony)))
 '(python-shell-interpreter "python3"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;;=================================================================================
;;== Melpa package system ==
;;=================================================================================
(require 'package)  
(setq package-enable-at-startup nil )
(add-to-list 'package-archives
             '("melpa-stable" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
	     '("org" . "http://orgmode.org/elpa/") t) ; Org-mode's repository
(package-initialize)
;;=================================================================================
;; === use-package setup ===
;;=================================================================================
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
        
(eval-when-compile
  (require 'use-package)
  (setq use-package-always-ensure t)
  )
(require 'diminish)
(require 'bind-key)
;;=================================================================================
;;end melpa
;;=================================================================================

;;=================================================================================
;; == Global settings ==
;;=================================================================================
;; Magit
(global-set-key (kbd "C-x g") 'magit-status)

;; Helm
(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(helm-mode 1)
;; TODO Hippie expand DOESN'T WORK
(global-set-key (kbd "S-<spc>") 'hippie-expand)
;;=================================================================================
;;end Global settings
;;=================================================================================


;;========================================================================
;; org-mode: the following lines are always needed.  Choose your own keys.
;;========================================================================
(use-package org
  :init
  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
  (global-set-key "\C-cl" 'org-store-link)
  (global-set-key "\C-ca" 'org-agenda)
  (global-set-key "\C-cb" 'org-iswitchb)
  (add-hook 'org-mode-hook 'org-indent-mode)
  (add-hook 'org-mode-hook 'visual-line-mode)
  (setq org-default-notes-file "~/org/notes.org")
  ;; Set default column view headings: Task Total-Time Time-Stamp
  (setq org-columns-default-format "%50ITEM(Task) %10CLOCKSUM %16TIMESTAMP_IA")
  (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                 (org-agenda-files :maxlevel . 9))))
  ;; capture setup
  (define-key global-map "\C-cc" 'org-capture)
  (setq org-capture-templates
	'(
	  ("t" "Todo" entry (file+headline org-default-notes-file  "Tasks")
           "* TODO %?\n%u\n%a\n" :clock-in t :clock-resume t) 
	  ("m" "Meeting" entry (file+headline org-default-notes-file "Meetings")
	   "* MEETING with %? :MEETING:\n%t" :clock-in t :clock-resume t)
	  ("n" "Next Task" entry (file+headline org-default-notes-file "Tasks")
	   "** NEXT %? \nDEADLINE: %t") ))
  ;;tags list
  (setq org-tag-alist '((:startgroup . nil)
			("@work" . ?w) ("@home" . ?h)
			(:endgroup . nil)
			("@emacs". ?e)
			("@browser". ?b)
			("@read/review". ?r)
			("@latex". ?t)
			("@programming" . ?p)
			("@errands" . ?o)
			))
  ;;TODO sequence
  (setq org-todo-keywords
	'((sequence "NEXT" "TODO" "IN PROGRESS" "WAITING" "|" "SUSPENDED" "DONE" )))
  ;;agenda custom functions
  (setq org-agenda-custom-commands 
	'(("g". "GTD contexts")
	  ("gb" "Browsing" tags-todo "@browser"
           ((org-agenda-overriding-header "Browsing")
            (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
	    ))
	  ("gp" "Programming" tags-todo "@programming"
           ((org-agenda-overriding-header "Programming")
            (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
	    ))
	  ("gr" "Read/Study" tags-todo "@read/review"
           ((org-agenda-overriding-header "Read/review")
            (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
	    ))
	  ("gv" "Writing" tags-todo "@writing"
           ((org-agenda-overriding-header "Writing")
            (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)
	    ))
	  )
	)
  (defun my-org-agenda-skip-all-siblings-but-first ()
    "Skip all but the first non-done entry."
    (let (should-skip-entry)
      (unless (org-current-is-todo)
	(setq should-skip-entry t))
      (save-excursion
	(while (and (not should-skip-entry) (org-goto-sibling t))
          (when (org-current-is-todo)
            (setq should-skip-entry t))))
      (when should-skip-entry
	(or (outline-next-heading)
            (goto-char (point-max))))))
  
  (defun org-current-is-todo ()
    (string= "TODO" (org-get-todo-state)))
)
;;=================================================================================
;;end org-mode
;;=================================================================================


;;=================================================================================
;; == helm ==
;;=================================================================================
(use-package helm)
;;=================================================================================
;; end helm
;;=================================================================================


;;=================================================================================
;; == company-mode ==
;;=================================================================================
(use-package company
  :ensure t
  :init (add-hook 'after-init-hook 'global-company-mode)
  :config
  (use-package company-irony :ensure t :defer t)
  (setq company-idle-delay              nil
	company-minimum-prefix-length   2
	company-show-numbers            t
	company-tooltip-limit           20
	company-dabbrev-downcase        nil
	company-backends                '((company-irony company-gtags))
	)
  :bind ("C-," . company-complete-common)
  )
;;=================================================================================
;;end company
;;=================================================================================


;;=================================================================================
;; == irony-mode ==
;; Company backend for C/C++
;;=================================================================================
(use-package irony
  :ensure t
  :defer t
  :init
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  :config
  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's function
  (defun my-irony-mode-hook ()
    (define-key irony-mode-map [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      'irony-completion-at-point-async))
  (add-hook 'irony-mode-hook 'my-irony-mode-hook)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
  )
;;=================================================================================
;;end irony mode
;;=================================================================================


;;=================================================================================
;;== python environment with elpy ==
;;=================================================================================
(use-package elpy
  :commands elpy-enable
  :init (with-eval-after-load 'python (elpy-enable))
  :config
  (progn
    (setq elpy-rpc-python-command "python3")
    (setq elpy-rpc-backend "jedi"
          elpy-rpc-project-specific 't)
    )
  (when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))
  )
;;=================================================================================
;;end elpy
;;=================================================================================


;;=================================================================================
;;gnuplot mode
;;=================================================================================
(use-package gnuplot-mode
  :ensure t
  :mode "\\.plt$")

;;=================================================================================
;;end gnuplot mode
;;=================================================================================


;;=================================================================================
;;rust mode
;;=================================================================================
;; (use-package rust-mode
;;   :defer t
;;   :mode "\\.rs\\'"
;;   :config (setq rust-format-on-save t))
;; (use-package cargo
;;   :init
;;   ;;(add-hook 'rust-mode-hook
;;   ;;(lambda ()
;; 	      ;;(local-set-key (kbf "C-c <tab>") #'rust-format-buffer)))
;;   (add-hook 'rust-mode-hook 'cargo-minor-mode)
;;   (add-hook 'toml-mode-hook 'cargo-minor-mode)
;;   )
;; ;;(use-package flycheck-rust
;; ;;  :init
;; ;;  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
;; (use-package racer
;;   :defer t
;;   :init
;;   (setq racer-cmd "~/.cargo/bin/racer")
;;   (setq racer-rust-src-path "~/rust/src") ;; Rust source code PATH
;;   (add-hook 'rust-mode-hook #'racer-mode)
;;   (add-hook 'racer-mode-hook #'eldoc-mode)
;;   (add-hook 'racer-mode-hook #'company-mode)
;;   (define-key rust-mode-map (kbd "C-c ,") #'company-indent-or-complete-common)
;;   )
;;=================================================================================
;;end rust mode
;;=================================================================================


;;=================================================================================
;;== julia + ess 
;;=================================================================================
 (use-package ess
  :ensure t
  :init (require 'ess-site))
;;=================================================================================
;;end julia + ess
;;=================================================================================


;;=================================================================================
;;== Markdown major mode 
;;=================================================================================
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init
  (setq markdown-command "pandoc")
  )
;;=================================================================================
;;end Markdown major mode
;;=================================================================================


;;=================================================================================
;;== LaTeX (AUCTeX) major mode 
;;=================================================================================
(use-package tex
  :mode (("\\.tex$" . latex-mode))
  :defer t
  :ensure auctex
  :config
  (add-hook 'text-mode-hook 'turn-on-visual-line-mode)
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil);
  (cua-selection-mode t)
  )
;;=================================================================================
;;end LaTeX major mode 
;;=================================================================================


;;=================================================================================
;;YASnippets mode 
;;=================================================================================
(use-package yasnippet
  :config
  (add-to-list 'load-path
              "~/.emacs.d/plugins/yasnippet")
  (yas-global-mode 1)
 )
;;=================================================================================
;;end YASnippets mode 
;;=================================================================================


;;=================================================================================
;;== Magit  
;;=================================================================================
(use-package magit
  :ensure t
  :defer t
  )
;;=================================================================================
;;== end Magit  
;;=================================================================================


;;=================================================================================
;;== ox-reveal  
;;=================================================================================
(use-package ox-reveal
:ensure ox-reveal)

(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
(setq org-reveal-mathjax t)

(use-package htmlize
:ensure t)
;;=================================================================================
;;== end ox-reveal  
;;=================================================================================
