#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export VISUAL="emacs"

#aliases for dangerous commands
alias rm='rm -i'
alias mv='mv -i'
HOME='/home/danilo'
#aliases for convenience
alias atlas='sudo bash ${HOME}/script/atlas_mnt'
#alias landau='bash ${HOME}/script/landau'
alias work='bash ${HOME}/script/remotestatus.sh ${HOME}/dotfiles/.git_repos_work.txt'
alias status='bash ${HOME}/script/remotestatus.sh ${HOME}/dotfiles/.git_repos_base.txt'
alias dots='bash ${HOME}/script/update_dotfiles_folder.sh'

#emacsclient #NOT USED ON LANDAU
#alias emacs='emacsclient -create-frame --alternate-editor=""'
#export ALTERNATE_EDITOR=""
#export EDITOR="emacsclient -c"                  # $EDITOR opens in terminal
#export VISUAL="emacsclient -c -a emacs"         # $VISUAL opens in GUI mode

#more history info
HISTTIMEFORMAT="%F %T  "
